import cheerio from 'cheerio';
import axios from 'axios';
import { map } from 'lodash';

const getURL = async (url) =>{
  const req = (await axios.get(url)).data;
  const list = [];
  const result = [];
  const html = cheerio.load(req);
  const links = html('a');
  html(links).each(function(i, link){
    list.push(html(link).attr('href'));
  });
  await Promise.all(
    map(list, async item => {
      if (item[0] !== 'h') return;
      const ststusCode =(await axios.get(item)).status;
      return result.push({ URL: item, ststusCode });
    }));
  return console.log('URLs and statusCosds -> ',result, 'URLs and statusCosds lengt', result.length);
};

getURL('https://www.google.com');
